﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClient.DataHelper
{
    public interface ISqlHelper
    {
        string ConnectionString { get; set; }
        DataTable ExecuteProc(string procName, Dictionary<string, string> values);
        object ExecuteScalarFunction(string sqlFunction, Dictionary<string, string> values);
        DataTable ExecuteTableFunction(string sqlFunction, Dictionary<string, string> values);
        List<DataRow> ExecuteSQL(string sql);
        int ExecuteNonQuery(string sql);
        object ExecuteScalar(string procName, Dictionary<string, string> values);
        T GetValue<T>(string sql, T defaultValue);

        bool BulkLoad(DataTable data, string DestinationTable);

    }
}
