﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClient.DataHelper.Extensions
{
    public static class DataTableExtensions
    {

        public static string GenerateScript(this DataTable table, string tableName)
        {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.Append("IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].["+ tableName+"]') AND type in (N'U')) \n");
            sqlQuery.Append("DROP TABLE [dbo].["+ tableName +"] \n");

            sqlQuery.Append("CREATE TABLE " + tableName + "(");
            for (int i = 0; i < table.Columns.Count; i++)
            {
                sqlQuery.Append("\n [" + table.Columns[i].ColumnName + "] ");
                string columnType = table.Columns[i].DataType.ToString();
                switch (columnType)
                {
                    case "System.Int32":
                        sqlQuery.Append(" int ");
                        break;
                    case "System.Int64":
                        sqlQuery.Append(" bigint ");
                        break;
                    case "System.Int16":
                        sqlQuery.Append(" smallint");
                        break;
                    case "System.Byte":
                        sqlQuery.Append(" tinyint");
                        break;
                    case "System.Decimal":
                        sqlQuery.Append(" decimal ");
                        break;
                    case "System.DateTime":
                        sqlQuery.Append(" datetime ");
                        break;
                    case "System.String":
                    default:
                        sqlQuery.Append(" nvarchar(500) ");
                        break;
                }
                if (table.Columns[i].AutoIncrement)
                    sqlQuery.Append(" IDENTITY(" + table.Columns[i].AutoIncrementSeed.ToString() + "," + table.Columns[i].AutoIncrementStep.ToString() + ") ");
                if (!table.Columns[i].AllowDBNull)
                    sqlQuery.Append(" NOT NULL ");
                sqlQuery.Append(",");
            }
            string sql = sqlQuery.ToString();
            return sql.Substring(0, sql.Length - 1) + "\n)";
        }

        public static string GetTableName(this string fileName)
        {
            int dot = fileName.LastIndexOf(".");
            int slash = fileName.LastIndexOf("\\") + 1;

            string tableName = $"{fileName[slash..dot].Replace(" ","")}";
            return tableName;
        }
        
    }
}
