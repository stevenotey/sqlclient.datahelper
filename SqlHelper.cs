﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SqlClient.DataHelper
{
    public class SqlHelper : ISqlHelper
    {
        public string ConnectionString { get; set; }
        public SqlHelper(string connectionString)

        {
            ConnectionString = connectionString;
        }

        #region STORED PROCS

        /// <summary>
        /// Builds a datatable from a stored proc
        /// </summary>
        /// <param name="procName">The SQL Proc to Execute</param>
        /// <param name="values">sql parameters that will update outputs</param>
        /// <returns>DataTable and updated output parameters in values</returns>
        public DataTable ExecuteProc(string procName, Dictionary<string, string> values)
        {
            DataTable dataTable = new DataTable(procName);

            SqlCommand cmd = new SqlCommand(procName, new SqlConnection(ConnectionString))
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Connection.Open();
            BuildParameters(ref cmd, values);

            SqlDataAdapter sqlDa = new SqlDataAdapter
            {
                SelectCommand = cmd
            };
            sqlDa.Fill(dataTable);
            cmd.Connection.Close();

            //dataSet = GetOutputParameters(dataSet, cmd);
            UpdateParameters(values, cmd);

            return dataTable;
        }
        /// <summary>
        /// Executes the query, and returns the first column of the first row in the result set returned by the query. 
        /// Additional columns or rows are ignored.
        /// </summary>
        /// <param name="procName">Stored Procdure</param>
        /// <param name="values">Dictionary of values</param>
        /// <returns>number of records effected</returns>
        public object ExecuteScalar(string procName, Dictionary<string, string> values)
        {
            SqlCommand cmd = new SqlCommand(procName, new SqlConnection(ConnectionString))
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Connection.Open();
            BuildParameters(ref cmd, values);

            var result = cmd.ExecuteScalar();

            cmd.Connection.Close();

            return result;
        }
        /// <summary>
        /// Executes a Transact-SQL statement against the connection and returns the number of rows affected.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        /// <remarks>
        /// </remarks>
        public int ExecuteNonQuery(string sql)
        {
            int records = 0;
            try
            {
                SqlCommand cmd = new SqlCommand(sql, new SqlConnection(ConnectionString))
                {
                    CommandType = CommandType.Text
                };

                if (sql.ToLower().Contains("select count(*)")) //for counting records
                {
                    cmd.Connection.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dr.Read();
                    records = dr.GetInt32(0);
                    cmd.Connection.Close();
                }
                else //insert,update,delete
                {
                    cmd.Connection.Open();
                    records = cmd.ExecuteNonQuery();
                    cmd.Connection.Close();
                }
            }
            catch (Exception x)
            {
                throw new Exception(x.Message);
            }
            return records;
        }


        /// <summary>
        /// Get the data table version of this stored proc
        /// </summary>
        /// <param name="procName">Stored Procedure name</param>
        /// <param name="values">Dictionary of name value pairs as parameters</param>
        /// <returns>DataTable</returns>
        public DataTable GetDataTable(string procName, Dictionary<string, string> values)
        {
            return ExecuteProc(procName, values);
        }

        public List<DataRow> GetRows(string procName, Dictionary<string, string> values)
        {
            return GetDataTable(procName, values).AsEnumerable().ToList();
        }


        #endregion

        #region SQL Statements

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sql"></param>
        /// <returns>List of DataRows</returns>
        public List<DataRow> ExecuteSQL(string sql)
        {
            DataTable dataTable = new DataTable("BVSDataTable");

            try
            {
                SqlCommand cmd = new SqlCommand(sql, new SqlConnection(ConnectionString))
                {
                    CommandType = CommandType.Text
                };

                cmd.Connection.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter
                {
                    SelectCommand = cmd
                };
                sqlDa.Fill(dataTable);
                cmd.Connection.Close();
            }
            catch (Exception x)
            {
                throw new Exception(x.Message);
            }

            return dataTable.AsEnumerable().ToList();
        }

        /// <summary>
        /// Generic funciton to return a value
        /// </summary>
        /// <typeparam name="T">value type</typeparam>
        /// <param name="sql">sql statement with 1 value</param>
        /// <param name="defaultValue">the default value to return if needed</param>
        /// <returns></returns>
        public T GetValue<T>(string sql, T defaultValue)
        {
            T value = defaultValue;

            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(sql, cn);
                    cn.Open();
                    var rv = cmd.ExecuteScalar();
                    cn.Close();

                    value = (T)rv;
                }
                catch
                {

                    value = defaultValue;
                }
            }
            return value;
        }



        #endregion

        #region SQL Functions
        /// <summary>
        /// Get datatable values from a sql function
        /// </summary>
        /// <param name="sqlFunction">the function you want to call</param>
        /// <param name="values">list of paraemters for the function</param>
        /// <returns>DataTable from based on the sql function</returns>
        public DataTable ExecuteTableFunction(string sqlFunction, Dictionary<string, string> values)
        {
            DataTable dataTable = new DataTable(sqlFunction.Replace("dbo.", ""));

            string sql = $"select * from {sqlFunction}({BuildFunctionParameters(values)})";

            SqlCommand cmd = new SqlCommand(sql, new SqlConnection(ConnectionString))
            {
                CommandType = CommandType.Text
            };

            //fill parameters
            foreach (KeyValuePair<string, string> v in values)
            {
                cmd.Parameters.AddWithValue(v.Key, v.Value);
            }

            cmd.Connection.Open();

            SqlDataAdapter sqlDa = new SqlDataAdapter
            {
                SelectCommand = cmd
            };
            sqlDa.Fill(dataTable);

            cmd.Connection.Close();

            return dataTable;
        }
        /// <summary>
        /// Executes sql user defined function
        /// </summary>
        /// <param name="sql">sql function you want to call</param>
        /// <param name="values">parameters supplied to the funciton</param>
        /// <returns>and object to be unboxed/parsed</returns>
        public object ExecuteScalarFunction(string sqlFunction, Dictionary<string, string> values)
        {
            string sql = $"select {sqlFunction}({BuildFunctionParameters(values)})";

            SqlCommand cmd = new SqlCommand(sql, new SqlConnection(ConnectionString))
            {
                CommandType = CommandType.Text
            };

            //fill parameters
            foreach (KeyValuePair<string, string> v in values)
            {
                cmd.Parameters.AddWithValue(v.Key, v.Value);
            }

            cmd.Connection.Open();

            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            var value = dr.GetString(0);
            cmd.Connection.Close();

            return value;
        }
        #endregion

        #region Supporting Functions

        /// <summary>
        /// Build the parameters for the sql function
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        protected string BuildFunctionParameters(Dictionary<string, string> values)
        {
            string s = string.Empty;

            foreach (KeyValuePair<string, string> v in values)
            {
                s = s + v.Key + ",";
            }

            return s.Remove(s.LastIndexOf(","));
        }
        /// <summary>
        /// Builds parameters for the stored procs
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <param name="values"></param>
        protected void BuildParameters(ref SqlCommand sqlCommand, Dictionary<string, string> values)
        {
            //skip if no parameters passed in
            if (values != null)
            {
                //get all the parameters from the stored proc
                SqlCommandBuilder.DeriveParameters(sqlCommand);

                //remove parameters not needed
                for (int i = sqlCommand.Parameters.Count - 1; i >= 0; i--)
                {
                    SqlParameter p = sqlCommand.Parameters[i];

                    if (!values.Keys.Contains(p.ParameterName))
                    {
                        sqlCommand.Parameters.RemoveAt(i);
                    }
                }

                //build only the parameters needed                
                foreach (SqlParameter p in sqlCommand.Parameters)
                {
                    switch (p.Direction)
                    {
                        case ParameterDirection.Input:
                            try
                            {
                                if (sqlCommand.Parameters[p.ParameterName].SqlDbType == SqlDbType.UniqueIdentifier)
                                {
                                    sqlCommand.Parameters[p.ParameterName].Value = new Guid(values[p.ParameterName].ToString());
                                }
                                else
                                {
                                    //https://docs.microsoft.com/en-us/archive/msdn-magazine/2014/october/csharp-the-new-and-improved-csharp-6-0#null-conditional-operator
                                    sqlCommand.Parameters[p.ParameterName].Value = values[p.ParameterName]?.ToString();
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Parameter Error [" + p.ParameterName + "] : " + ex.Message, ex);
                            }
                            break;

                        case ParameterDirection.InputOutput:
                            try
                            {
                                if (sqlCommand.Parameters[p.ParameterName].SqlDbType == SqlDbType.UniqueIdentifier)
                                {
                                    if (values[p.ParameterName].ToString() != "")
                                    {
                                        sqlCommand.Parameters[p.ParameterName].Value = values[p.ParameterName] == null ? Guid.Empty : new Guid(values[p.ParameterName].ToString());
                                    }
                                }
                                else
                                {
                                    sqlCommand.Parameters[p.ParameterName].Value = values[p.ParameterName]?.ToString();
                                }
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Parameter Error [" + p.ParameterName + "] : " + e.Message, e);
                            }
                            break;
                    }
                }
            }
        }
        /// <summary>
        /// Builds a DataSet object of paramenters returned by the Command Object
        /// </summary>
        /// <param name="ds">DataSet object</param>
        /// <param name="cmd">SqlCommand Object</param>
        protected static DataSet GetOutputParameters(DataSet dataSet, SqlCommand cmd)
        {
            DataSet ds = dataSet;
            if (ds.Tables.Count > 0)
            {
                DataTable dt = new DataTable("Output");
                dt.Columns.Add("Parameter");
                dt.Columns.Add("Value");
                ds.Tables.Add(dt);
                foreach (SqlParameter p in cmd.Parameters)
                {
                    if ((p.Direction == ParameterDirection.InputOutput) || (p.Direction == ParameterDirection.Output) || (p.Direction == ParameterDirection.ReturnValue))
                    {
                        DataRow dr = ds.Tables["Output"].NewRow();
                        dr["Parameter"] = p.ParameterName;
                        dr["Value"] = p.Value;
                        dt.Rows.Add(dr);
                    }
                }
            }
            return ds;
        }
        /// <summary>
        /// Update parameters if they require output
        /// </summary>
        /// <param name="values">Parameter List</param>
        /// <param name="cmd">SqlClient Command</param>
        /// <returns>Update hashtable of parameters</returns>
        protected static Dictionary<string, string> UpdateParameters(Dictionary<string, string> values, SqlCommand cmd)
        {
            Dictionary<string, string> returnValues = new Dictionary<string, string>();
            returnValues = values;

            foreach (SqlParameter p in cmd.Parameters)
            {
                if ((p.Direction == ParameterDirection.InputOutput) || (p.Direction == ParameterDirection.Output))
                {
                    returnValues[p.ParameterName] = p.Value.ToString();
                }
            }
            return returnValues;
        }

        /// <summary>
        /// Bulk load data into SQL Table
        /// </summary>
        /// <param name="data"></param>
        /// <param name="DestinationTable"></param>
        /// <returns>bool</returns>
        /// <remarks>Assumes that you are mapping 1:1 Datatable column header to Destination</remarks>
        public bool BulkLoad(DataTable data, string DestinationTable)
        {
            SqlBulkCopy sqlBulk = new SqlBulkCopy(ConnectionString)
            {
                DestinationTableName = DestinationTable
            };

            foreach (DataColumn c in data.Columns)
            {
                sqlBulk.ColumnMappings.Add(c.ColumnName, c.ColumnName);
            }

            sqlBulk.WriteToServer(data);

            return true;
        }

        #endregion


        #region System Tables

        /// <summary>
        /// Lists the tables in a schema
        /// </summary>
        /// <param name="schema">optional, defaults to dbo</param>
        /// <returns></returns>
        public List<string> GetTableNames(string schema = "dbo")
        {
            List<string> tableNames = new List<string>();

            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.Append("SELECT DISTINCT T.NAME AS TableName \n");
            sqlQuery.Append("FROM   sys.OBJECTS AS T \n");
            sqlQuery.Append("       JOIN sys.COLUMNS AS C \n");
            sqlQuery.Append("         ON T.OBJECT_ID = C.OBJECT_ID \n");
            sqlQuery.Append("       JOIN sys.TYPES AS P \n");
            sqlQuery.Append("         ON C.SYSTEM_TYPE_ID = P.SYSTEM_TYPE_ID \n");
            sqlQuery.Append("       JOIN sys.SCHEMAS S \n");
            sqlQuery.Append("         ON S.SCHEMA_ID = T.SCHEMA_ID \n");
            sqlQuery.Append("WHERE  T.TYPE_DESC = 'USER_TABLE' \n");
            sqlQuery.Append("       AND S.NAME = '" + schema + "'");

            var tables = ExecuteSQL(sqlQuery.ToString()).AsEnumerable().ToList();

            //tableNames.AddRange((IEnumerable<string>)(from t in tables select t.Field<string>("TableName").ToList()));

            foreach(var t in tables)
            {
                tableNames.Add(t.Field<string>("TableName"));
            }

            return tableNames;
        }

        /// <summary>
        /// Gets list of table columnms
        /// </summary>
        /// <param name="schema">optional, defaults to dbo</param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetTableColumns(string tableName, string schema = "dbo")
        {
            Dictionary<string, string> columnNames = new();

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT  C.name AS ColumnName \n");
            sql.Append("	   ,types.name DataType \n");
            sql.Append("FROM   sys.objects AS T \n");
            sql.Append("   JOIN sys.columns AS C ON T.object_id = C.object_id \n");
            sql.Append("   JOIN sys.SCHEMAS S ON S.SCHEMA_ID = T.SCHEMA_ID \n");
            sql.Append("   join sys.types types on types.system_type_id = c.system_type_id \n");
            sql.Append("WHERE  T.type_desc = 'USER_TABLE' \n");
            sql.Append("and t.name = '" + tableName + "' \n");
            sql.Append("AND S.NAME = '" + schema + "' \n");
            sql.Append("and types.name <> 'sysname' \n");
            sql.Append("ORDER  BY C.NAME ");

            var columns = ExecuteSQL(sql.ToString()).AsEnumerable().ToList();

            //tableNames.AddRange((IEnumerable<string>)(from t in tables select t.Field<string>("TableName").ToList()));

            foreach (var c in columns)
            {
                columnNames.Add(c.Field<string>("ColumnName"), c.Field<string>("DataType"));
            }

            return columnNames;
        }

        #endregion
    }
}
